# Getting access to the group server and installing software. 

#### 1. If you are going to access the server from off campus, you will need to install the LaTech VPN client. This will make the server think you are on campus. 

Go to https://techvpn.latech.edu/ and login with your LaTech username and password. Install the GlobalProtect agent for your OS. You can run that to connect to the VPN. This is not necessary if you are on campus. The portal is "techvpn.latech.edu".


#### 2. Run a remote desktop program. Windows includes "remote desktop connection".

Connect to 138.47.29.134

If you click the down arrow below the computer name you can input your username so that you don't have to enter it everytime. 

You should see a black screen with a window that says Login to xrdp. Change the username to your username and put in your password. 


#### 3. Open a terminal using ctrl-alt-t or by clicking on the icon that looks like a block box with $_ at the bottom of the screen. 



#### 4. Install MiniConda; a software suite for organizing python code. Execute the following code:




```bash
cd ~/Downloads #change directory to Downloads

wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh #Download file to install Miniconda

chmod +x Miniconda3-latest-Linux-x86_64.sh #allow the file to be run
```

#### 5. Run the file to install Conda


```bash
./Miniconda3-latest-Linux-x86_64.sh #run the file
```

Follow the instructions on the screen to install miniconda. Just press enter unless it reuires an answer, in which case just enter yes. 

Close and reopen the terminal. This allows the terminal to find conda now. 

#### 6. Set up HOOMD Environment
HOOMD 4.8 is installed on the server for all to use, however you need to set up the correct environment to use it. 

Create a new conda environment via:

```bash
conda create --name HOOMD4.8
```

Then activate the new environment and install the correct dependencies. 

```bash
conda activate HOOMD4.8
conda install cmake eigen git python numpy pybind11 gsd
```

Whenever you want to run HOOMD, you must first activate the HOOMD4.8 environment. 

More information can be found in the HOOMD documentation found here: https://hoomd-blue.readthedocs.io/en/v4.8.2/index.html


#### 7. Run hoomd tutorials. https://hoomd-blue.readthedocs.io/en/v4.8.2/tutorial/01-Introducing-Molecular-Dynamics/00-index.html

HOOMD runs in python. In the terminal type python, then you will be able to enter to code in the tutorials. Do the Molecular Dynamics tutorials. 

For the tutorials, enter code into python in the terminal is fine. When you actually running simualtions you will want to create a script and run the script with


```bash
python script.py
```

In the terminal, where "script.py" is the name of the script. Everything you enter in the terminal in the tutorials will need to be in the script. When you're actually running simulations, make sure you save every script you run so we can refer back to it later. 

#### 8. LAMMPS
LAMMPS is alreads in installed on the system and can be run using
```bash
lmp_stable -in file.in
```

or 

```bash
mpirun -np 4 lmp_stable -in file.in #runs on 4 cores
```
to run on multiple cores.

Tutorials can be found here:
https://lammps.sandia.gov/tutorials.html
https://icme.hpc.msstate.edu/mediawiki/index.php/LAMMPS_tutorials.html

and the documentation can be found here:
https://lammps.sandia.gov/doc/Manual.html

It may be necessary to install your own version of LAMMPS to take advantage of all the features available.

## Other Software

#### VMD
VMD, which is used to view simulation results, can be run from the terminal using:


```bash
vmd name.gsd
```

where name.gsd is the name of the gsd file you want to open. 

#### MATLAB
MATLAB will also be useful. I recommend creating a folder called MATLAB in your home directory where you store all you code, for example ~/MATLAB. That can be done like this:


```bash
cd ~
mkdir MATLAB
cd MATLAB
```

Then, download some common code our group uses using git. All the code is available here: https://gitlab.com/MATLAB-HOOMD-System. It is organized into repositories. I recommend creating a GIT directory in your MATLAB directory to store this code. 


```bash
cd ~/MATLAB
mkdir GIT
cd GIT
```

Each repository can be downloaded and stored in your MATLAB directory via:


```bash
cd ~/MATLAB/GIT
git clone https://gitlab.com/MATLAB-HOOMD-System/System.git
```

where you can substitue nameofrepository.git in for System.git. I recommend downloading:

Build.git

System.git

Analysis.git

miscellaneous.git

matlab_gsd_reader.git

Other repositories can be download if relavent to your project. 

For the matlab_gsd_reader repository, specific python code (generally done by using a conda environment) is required. Follow the instructions in the readme [here.](https://gitlab.com/MATLAB-HOOMD-System/matlab_gsd_reader/-/blob/master/readme.md)


To make these files available to MATLAB (that is, add them to your path) when MATLAB starts we need to create a file called startup.m in our matlab directory and tell it do add those folder to the path. Run the following in the terminal:


```bash
cd ~/MATLAB
echo "addpath(genpath('~/MATLAB/GIT/'))\npy.sys.setdlopenflags(int32(10));" > startup.m
```

The "py.sys.setdlopenflags(int32(10))" command helps with the MATLAB gsd reader. 


## Bash
Bash is the language used in the terminal. A list of useful commands can be found here: https://gitlab.com/Andrew-Peters/getting_started_petersgroup/-/blob/master/bashshellcheatsheetv2-110831030047-phpapp01.pdf

## Data Storage

Data should be stored on the backed up large file storage system located at /mnt/RAID1/Insync/Shared_Data

You will have your own folder with your username in that directory where you can store your data. 
